import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../environments/environment';
import {Employee} from '../Models/Employee';
import {Appointment} from '../Models/Appointment';
import {EmployeeAddModel} from '../Models/Dtos/EmployeeAddModel';
import {MonthReport} from '../Models/MonthReport';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) {
  }


  getEmployeeList(): Promise<Employee[]> {
    return this.http.get<Employee[]>(environment.apiurl + '/Employee/').toPromise();
  }

  getEmployee(id: number): Promise<Employee> {
    return this.http.get<Employee>(environment.apiurl + '/Employee/' + id).toPromise();
  }

  getAppointments(id: number) {
    return this.http.get<Appointment[]>(environment.apiurl + '/Employee/GetAppointments?id=' + id).toPromise();
  }

  getMonthlyReports(id: number) {
    return this.http.get<MonthReport[]>(environment.apiurl + '/Employee/GetReportsByMonth?idEmployee=' + id).toPromise();
  }

  getFutureAppointments(id: number) {
    return this.http.get<Appointment[]>(environment.apiurl + '/Employee/GetFutureAppointments?id=' + id).toPromise();
  }

  deleteEmployee(id: number) {
    return this.http.delete(environment.apiurl + '/Employee?id=' + id).toPromise();
  }

  upsertEmploye(id: number, employee: EmployeeAddModel) {
    return this.http.put(environment.apiurl + '/Employee?id=' + id, employee).toPromise();
  }

  addEmployee(employee: EmployeeAddModel) {
    return this.http.post(environment.apiurl + '/Employee', employee).toPromise();

  }
}

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../environments/environment';
import {Service} from '../Models/Service';
import {ServiceAddModel} from '../Models/Dtos/ServiceAddModel';


@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http: HttpClient) {
  }


  getServicesList(): Promise<Service[]> {
    return this.http.get<Service[]>(environment.apiurl + '/Service/').toPromise();
  }


  deleteService(id: number) {
    return this.http.delete(environment.apiurl + '/Service?id=' + id).toPromise();
  }

  updateService(id: number, serviceParam: ServiceAddModel) {
    return this.http.put(environment.apiurl + '/Service?id=' + id, serviceParam).toPromise();
  }

  getServicesByEmployee(employee: number) {
    return this.http.get<Service[]>(environment.apiurl + '/Service/GetServicesByEmployee?id=' + employee).toPromise();
  }

  addService(service: ServiceAddModel) {
    return this.http.post(environment.apiurl + '/Service', service).toPromise();
  }

}

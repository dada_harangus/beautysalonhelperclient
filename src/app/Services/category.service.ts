import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Category} from '../Models/Category';
import {environment} from '../environments/environment';
import {Service} from '../Models/Service';


@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) {
  }


  getCategoryList() {
    return this.http.get<Category[]>(environment.apiurl + '/Category/').toPromise();
  }


  deleteCategory(id: number) {
    return this.http.delete(environment.apiurl + '/Category?id=' + id).toPromise();
  }

  updateCategory(id: number, category: Category) {
    return this.http.put(environment.apiurl + '/Category?id=' + id, category).toPromise();
  }

  addCategory(category: Category) {
    return this.http.post(environment.apiurl + '/Category', category).toPromise();

  }

  getServices(id: number) {
    return this.http.get<Service[]>(environment.apiurl + '/service/GetByCategory?id=' + id).toPromise();

  }
}

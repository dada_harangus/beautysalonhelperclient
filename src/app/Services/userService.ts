import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../Models/User';
import {environment} from '../environments/environment';


@Injectable({providedIn: 'root'})
export class UserService {
  constructor(private http: HttpClient) {
  }

  getAll() {
    return this.http.get <User[]>(environment.apiurl + '/users').toPromise();
  }

  changeRole(id: number, role: string) {
    return this.http.get(environment.apiurl + '/users/changeRole?id=' + id + '&role=' + role).toPromise();
  }

  deleteClient(id: number) {
    return this.http.delete(environment.apiurl + '/users?id=' + id).toPromise();
  }
}

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Category} from '../Models/Category';
import {environment} from '../environments/environment';
import {Appointment} from '../Models/Appointment';
import {AppointmentAddModel} from '../Models/Dtos/AppointmentAddModel';

@Injectable({
  providedIn: 'root'
})
export class AppointmentService {

  constructor(private http: HttpClient) {
  }


  getAppointmentList(): Promise<Appointment[]> {
    return this.http.get<Appointment[]>(environment.apiurl + '/Appointment/').toPromise();
  }

  getAppointmentsByDate(date: string): Promise<Appointment[]> {
    return this.http.get<Appointment[]>(environment.apiurl + '/Appointment/getAppointmentsByDate?date=' + date).toPromise();
  }


  deleteAppointment(id: number) {
    return this.http.delete(environment.apiurl + '/Appointment?id=' + id).toPromise();
  }

  addAppointment(appointment: AppointmentAddModel) {
    return this.http.post(environment.apiurl + '/Appointment', appointment).toPromise();
  }

  upsertAppointment(id: number, appoinment: Appointment) {
    return this.http.put(environment.apiurl + '/Appointment?id=' + id, appoinment).toPromise();
  }
}

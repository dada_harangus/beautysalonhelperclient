import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../environments/environment';
import {Client} from '../Models/Client';
import {Appointment} from '../Models/Appointment';
import {ClientAddModel} from '../Models/Dtos/ClientAddModel';
import {MonthReport} from '../Models/MonthReport';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) {
  }


  getClientList(): Promise<Client[]> {
    return this.http.get<Client[]>(environment.apiurl + '/Client/').toPromise();
  }

  search(searchTerm: string): Promise<ClientAddModel[]> {
    return this.http.get<ClientAddModel[]>(environment.apiurl + '/Client/Search?name=' + searchTerm).toPromise();
  }

  getClient(id: number): Promise<ClientAddModel> {
    return this.http.get<ClientAddModel>(environment.apiurl + '/Client/' + id).toPromise();
  }

  addClient(client: ClientAddModel) {
    return this.http.post(environment.apiurl + '/Client', client);
  }

  deleteClient(id: number) {
    return this.http.delete(environment.apiurl + '/Client?id=' + id).toPromise();
  }

  updateClient(id: number, client: ClientAddModel) {
    console.log(client);
    return this.http.put(environment.apiurl + '/Client?id=' + id, client).toPromise();
  }

  getAppointments(id: number) {
    return this.http.get<Appointment[]>(environment.apiurl + '/Client/GetAppointments?id=' + id).toPromise();
  }

  getFutureAppointments(id: number) {
    return this.http.get<Appointment[]>(environment.apiurl + '/Client/GetFutureAppointments?id=' + id).toPromise();
  }

  getMonthlyReports(id: number) {
    return this.http.get<MonthReport[]>(environment.apiurl + '/Client/GetReportsByMonth?idClient=' + id).toPromise();
  }
}

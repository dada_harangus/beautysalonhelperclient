import {Component, EventEmitter, Output} from '@angular/core';
import {MatDialog} from '@angular/material';
import {DialogConfirmationDeleteInputComponent} from './dialogConfirmationDeleteInput';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'confirmationDeleteComponent',
  templateUrl: './dialogConfirmationDelete.html',
  styleUrls: ['./dialogConfirmationDelete.css']
})
export class DialogConfirmationDeleteComponent {

  constructor(public dialog: MatDialog) {
  }

  @Output() yes = new EventEmitter<boolean>();

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogConfirmationDeleteInputComponent, {
      width: '350px',
      data: 'Do you confirm the deletion of this data?'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        this.yes.emit(true);
      }
    });
  }
}

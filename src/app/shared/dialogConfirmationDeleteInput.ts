import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'DialogConfirmationDeleteInputComponent',
  templateUrl: './dialogConfirmationDeteleInput.html',

})
export class DialogConfirmationDeleteInputComponent {
  constructor(
    public dialogRef: MatDialogRef<DialogConfirmationDeleteInputComponent>,
    @Inject(MAT_DIALOG_DATA) public message: string) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

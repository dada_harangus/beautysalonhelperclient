import {Service} from './Service';

export class CategoryWithServices {


  id: number;
  name: string;
  services: Service[];

  constructor(id: number, name: string, services: Service[]) {
    this.id = id;
    this.name = name;
    this.services = services;

  }
}

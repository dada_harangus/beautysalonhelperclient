export class Client {
  id: number;
  clientName: string;
  telephoneNumber: number;
  email: string;
  description: string;

}

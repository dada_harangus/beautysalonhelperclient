export class RegisterPostModel {
  firstName: string;
  lastName: string;
  username: string;
  password: string;
}

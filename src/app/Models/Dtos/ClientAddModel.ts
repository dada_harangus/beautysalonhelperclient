export class ClientAddModel {
  id: number;
  clientName: string;
  telephoneNumber: number;
  email: string;
  description: string;

}

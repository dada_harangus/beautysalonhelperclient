import {Category} from '../Category';

export class ServiceAddModel {
  name: string;
  price: number;
  duration: number;
  categoryId: number;
}

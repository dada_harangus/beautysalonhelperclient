import {ClientAddModel} from './ClientAddModel';
import {Employee} from '../Employee';
import {Service} from '../Service';
export class AppointmentAddModel {
  client: ClientAddModel;
  startTime: string;
  endTime: string;
  employee: Employee;
  services: Service[];
}

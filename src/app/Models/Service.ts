import {Category} from './Category';

export class Service {
  id: number;
  name: string;
  price: number;
  duration: number;
  category: Category;
}

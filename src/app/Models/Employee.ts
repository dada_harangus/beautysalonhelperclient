import {Category} from './Category';

export class Employee {
  id: number;
  name: string;
  category: Category;
}

import {Client} from './Client';
import {Employee} from './Employee';
import {Service} from './Service';
import {ClientAddModel} from './Dtos/ClientAddModel';

export class Appointment {
  id: number;
  client: ClientAddModel;
  startTime: string;
  endTime: string;
  employee: Employee;
  services: Service[];
}

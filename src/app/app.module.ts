import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppointmentComponent} from './Components/appointment/appointment.component';
import {EmployeeComponent} from './Components/employee/employee.component';
import {ClientComponent} from './Components/client/client.component';
import {CategoryComponent} from './Components/category/category.component';
import {ServiceComponent} from './Components/service/service.component';
import {
  MAT_SNACK_BAR_DEFAULT_OPTIONS,
  MatAutocompleteModule,
  MatButtonModule, MatCardModule, MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatFormFieldModule, MatGridListModule, MatIconModule,
  MatInputModule, MatListModule,
  MatMenuModule, MatProgressSpinnerModule, MatRadioModule, MatSelectModule, MatSnackBarModule, MatTableModule,
  MatToolbarModule
} from '@angular/material';
import {AppointmentService} from './Services/appointment.service';
import {CategoryService} from './Services/category.service';
import {ClientService} from './Services/client.service';
import {EmployeeService} from './Services/employee.service';
import {ServiceService} from './Services/service.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DialogEditComponent} from './Components/category/EditCategoryDialog/dialog-edit';
import {DialogAddServiceComponent} from './Components/service/DialogAddService/dialogAddService';
import {ClientAddDialogComponent} from './Components/client/clientDialog/clientAddDialog';
import {ClientDetailComponent} from './Components/client/client-detail/client-detail.component';
import {EmployeeDetailComponent} from './Components/employee/employee-detail/employee-detail.component';
import {EmployeeAddDialogComponent} from './Components/employee/employeeAddDialog/employeeAddDialog';
import {DialogEditInputComponent} from './Components/category/EditCategoryDialog/dialog-Edit-input';
import {DialogAddServiceInputComponent} from './Components/service/DialogAddService/dialogAddServiceInput';
import {DialogConfirmationDeleteComponent} from './shared/dialogConfirmationDelete';
import {DialogConfirmationDeleteInputComponent} from './shared/dialogConfirmationDeleteInput';
import {EmployeeAddDialogInputComponent} from './Components/employee/employeeAddDialog/employeeAddDialogInput';
import {ClientAddDialogInputComponent} from './Components/client/clientDialog/clientAddDialogInput';
import {DialogAppoinmentInputComponent} from './Components/appointment/dialogAppoiment/dialogAppoinmentInput';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {FlexLayoutModule} from '@angular/flex-layout';
import {EditAppointmentComponent} from './Components/appointment/edit-appointment/edit-appointment.component';
import {AuthenticationService} from './Services/AuthenticationService';
import {UserService} from './Services/userService';
import {LoginComponent} from './Components/login/login.component';
import {JwtInterceptor} from './Helpers/JwtInterceptor';
import {ErrorInterceptor} from './ErrrorInterceptors/ErrorInterceptors';
import { HomeComponent } from './Components/home/home.component';
import {CommonModule} from '@angular/common';
import { RegisterComponent } from './Components/register/register.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {DialogChangeRoleComponent, UserComponent} from './Components/user/user.component';



@NgModule({
  declarations: [
    AppComponent,
    AppointmentComponent,
    EmployeeComponent,
    ClientComponent,
    CategoryComponent,
    ServiceComponent,
    ClientDetailComponent,
    DialogEditComponent,
    DialogEditInputComponent,
    DialogAddServiceComponent,
    DialogAddServiceInputComponent,
    ClientAddDialogComponent,
    ClientAddDialogInputComponent,
    EmployeeDetailComponent,
    EmployeeAddDialogComponent,
    EmployeeAddDialogInputComponent,
    DialogConfirmationDeleteComponent,
    DialogConfirmationDeleteInputComponent,
    DialogAppoinmentInputComponent,
    EditAppointmentComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    UserComponent,
    DialogChangeRoleComponent
  ],
  entryComponents: [ DialogAddServiceInputComponent,
    ClientAddDialogInputComponent, EmployeeAddDialogInputComponent, DialogEditInputComponent, DialogConfirmationDeleteInputComponent
    , DialogAppoinmentInputComponent, DialogChangeRoleComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatMenuModule,
    MatToolbarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatExpansionModule,
    MatInputModule,
    HttpClientModule,
    FormsModule,
    MatDialogModule,
    MatListModule,
    MatCardModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatGridListModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatRadioModule,
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatDialogModule,
    MatTableModule,
    MatMenuModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatAutocompleteModule,
    MatSnackBarModule



  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 2500}},
    AppointmentService,
    CategoryService,
    ClientService,
    EmployeeService,
    ServiceService,
    AuthenticationService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

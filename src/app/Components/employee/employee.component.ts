import {Component, OnInit} from '@angular/core';
import {Employee} from '../../Models/Employee';
import {EmployeeService} from '../../Services/employee.service';


@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  employees: Employee[];
  add = 'Add';
  role = JSON.parse(localStorage.getItem('currentUser')).role;

  constructor(private service: EmployeeService) {
  }

  ngOnInit() {
    this.getEmployees();
  }

  getEmployees() {
    this.service.getEmployeeList().then(rsp => {
      this.employees = rsp;

    }, err => {
      console.log('error', err);
    });
  }

  refresh() {
    this.getEmployees();
  }
}

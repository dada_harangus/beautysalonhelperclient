import {Component,  Inject, OnInit} from '@angular/core';
import {Category} from '../../../Models/Category';
import {CategoryService} from '../../../Services/category.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DialogData} from './employeeAddDialog';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'EmployeeAddDialogInput',
  templateUrl: 'employeeAddDialogInput.html',
  styleUrls: ['employeeAddDialog.css'],
})
// tslint:disable-next-line:component-class-suffix
export class EmployeeAddDialogInputComponent implements OnInit {
  categories: Category[];

  constructor(private service: CategoryService,
              public dialogRef: MatDialogRef<EmployeeAddDialogInputComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    this.getCategories();
  }

  getCategories() {
    this.service.getCategoryList().then(rsp => {
      this.categories = rsp;
    });
  }


}

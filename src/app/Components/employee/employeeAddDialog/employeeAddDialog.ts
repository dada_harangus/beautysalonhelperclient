import {Component, EventEmitter, Input, Output,} from '@angular/core';
import {MatDialog} from '@angular/material';
import {Category} from '../../../Models/Category';
import {EmployeeAddDialogInputComponent} from './employeeAddDialogInput';
import {EmployeeAddModel} from '../../../Models/Dtos/EmployeeAddModel';
import {EmployeeService} from '../../../Services/employee.service';
import {Location} from '@angular/common';

export interface DialogData {

  employeeName: string;
  category: Category;
  telephoneNumber: number;
  nameError: any;
  categoryError: any;
  telephoneNumberError;
  name: string;

}

/**
 * @title Dialog Overview
 */
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'employeeAddDialog',
  templateUrl: 'employeeAddDialog.html',
  styleUrls: ['employeeAddDialog.css'],
})
// tslint:disable-next-line:component-class-suffix
export class EmployeeAddDialogComponent {


  name = JSON.parse(localStorage.getItem('currentUser')).username;
  nameError: any;
  categoryError: any;
  telephoneNumberError;
  category: Category;
  telephoneNumber: number;
  employeeName: string;
  @Input() addoredit: boolean;
  @Input() idEmployee: number;
  @Output() succes = new EventEmitter<boolean>();
  errors: any[];


  constructor(private dialog: MatDialog, private service: EmployeeService, private location: Location) {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(EmployeeAddDialogInputComponent, {
      width: '250px',
      data: {
        name: this.name,
        employeeName: this.employeeName,
        category: this.category,
        telephoneNumber: this.telephoneNumber,
        telephoneNumberError: this.telephoneNumberError,
        nameError: this.nameError,
        categoryError: this.categoryError
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.employeeName = result.employeeName;
        this.category = result.category;
        this.telephoneNumber = result.telephoneNumber;
        const employee = {
          nameEmployee: this.employeeName,
          category: this.category ? this.category.id : undefined,
          telephoneNumber: this.telephoneNumber
        } as EmployeeAddModel;

        if (this.addoredit === true) {
          this.service.upsertEmploye(this.idEmployee, employee).then(rsp => {
            this.succes.emit(true);
            this.clearData();
          }, err => {
            this.errors = err;
            this.setErrors();
            this.errors = null;
            this.openDialog();
          });
        }

        if (this.addoredit === false) {
          this.service.addEmployee(employee).then(rsp => {
            this.succes.emit(true);
            this.clearData();
          }, err => {
            this.errors = err;
            this.setErrors();
            this.errors = null;
            this.openDialog();
          });
        }
      }
    });
  }

  private clearData() {
    this.employeeName = null;
    this.telephoneNumber = null;
    this.category = null;
    this.nameError = null;
    this.categoryError = null;
    this.telephoneNumberError = null;
    this.errors = null;
  }

  private setErrors() {
    this.telephoneNumberError = this.errors.find(x => x.propertyName === 'TelephoneNumber')
      ?
      this.errors.find(x => x.propertyName === 'TelephoneNumber').errorMessage : undefined;
    this.nameError = this.errors.find(x => x.propertyName === 'nameEmployee')
      ? this.errors.find(x => x.propertyName === 'nameEmployee').errorMessage : undefined;
    this.categoryError = this.errors.find(x => x.propertyName === 'category')
      ? this.errors.find(x => x.propertyName === 'category').errorMessage : undefined;
  }

}


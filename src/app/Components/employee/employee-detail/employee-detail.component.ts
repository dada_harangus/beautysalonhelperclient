import {Component, OnInit} from '@angular/core';
import {Appointment} from '../../../Models/Appointment';
import {ActivatedRoute} from '@angular/router';
import {Employee} from '../../../Models/Employee';
import {EmployeeService} from '../../../Services/employee.service';
import {Location} from '@angular/common';
import {MonthReport} from '../../../Models/MonthReport';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit {
  role = JSON.parse(localStorage.getItem('currentUser')).role;
  monthReports: MonthReport[];
  edit = 'Edit';
  employee: Employee;
  history: Appointment [];
  future: Appointment[];

  constructor(private service: EmployeeService,
              private route: ActivatedRoute,
              private location: Location,
              private deleteAlert: MatSnackBar
  ) {
  }

  async ngOnInit(): Promise<void> {

    await this.getEmployee();
    this.getAppointments();
    this.getFutureAppointment();
    this.getMonthReports();
  }


  async getEmployee(): Promise<void> {
    const id = +this.route.snapshot.paramMap.get('id');
    this.employee = await this.service.getEmployee(id);
  }


  deleteEmployee(id: number, confirmed: boolean) {
    if (confirmed) {
      this.service.deleteEmployee(id).then(rsp => {
        if (rsp === false) {
          this.deleteAlert.open('Can`t delete employee because it has appointments . Delete appointment first');
        }
        this.location.back();
      }, err => {
        console.log(err);
      });

    }
  }

  private getAppointments() {
    this.service.getAppointments(this.employee.id).then(rsp =>
      this.history = rsp);
  }


  private getFutureAppointment() {
    this.service.getFutureAppointments(this.employee.id).then(rsp =>
      this.future = rsp);

  }

  private getMonthReports() {
    this.service.getMonthlyReports(this.employee.id).then(rsp =>
      this.monthReports = rsp);
  }

  refresh() {
    this.getEmployee();
  }
}

import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DialogData} from './clientAddDialog';
import {FormControl, Validators} from '@angular/forms';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'clientAddDialogInput',
  templateUrl: 'clientAddDialogInput.html',
  styleUrls: ['clientAddDialog.css'],
})
// tslint:disable-next-line:component-class-suffix
export class ClientAddDialogInputComponent {


  constructor(
    public dialogRef: MatDialogRef<ClientAddDialogInputComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }


  onNoClick(): void {

    this.dialogRef.close();
  }

}

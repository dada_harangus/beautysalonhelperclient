import {Component, EventEmitter, Input, Output} from '@angular/core';
import {MatDialog} from '@angular/material';
import {ClientAddDialogInputComponent} from './clientAddDialogInput';
import {first} from 'rxjs/operators';
import {ClientService} from '../../../Services/client.service';
import {Location} from '@angular/common';


export interface DialogData {
  clientName: string;
  telephoneNumber: number;
  email: string;
  description: string;
  name: string;
  telephoneNumberError: string;
  emailError: string;
  nameError: string;
}

/**
 * @title Dialog Overview
 */
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'clientAddDialog',
  templateUrl: 'clientAddDialog.html',
  styleUrls: ['clientAddDialog.css'],
})
// tslint:disable-next-line:component-class-suffix
export class ClientAddDialogComponent {
  errors: any[] = new Array(10);

  name = JSON.parse(localStorage.getItem('currentUser')).username;
  telephoneNumber: number;
  email: string;
  description: string;
  clientName: string;
  @Input() addoredit: boolean;
  telephoneNumberError: string;
  emailError: string;
  nameError: string;
  @Input() idClient: number;
  @Output() succes = new EventEmitter<boolean>();

  constructor(public dialog: MatDialog, public service: ClientService, public location: Location) {

  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ClientAddDialogInputComponent, {
      width: '250px',
      data: {
        name: this.name,
        clientName: this.clientName,
        telephoneNumber: this.telephoneNumber,
        email: this.email,
        description: this.description,
        emailError: this.emailError,
        telephoneNumberError: this.telephoneNumberError,
        nameError: this.nameError
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.clientName = result.clientName;
        this.telephoneNumber = result.telephoneNumber;
        this.email = result.email;
        this.description = result.description;
        const client = {
          id: 0,
          name: this.name,
          clientName: this.clientName,
          telephoneNumber: this.telephoneNumber,
          email: this.email,
          description: this.description
        };

        if (this.addoredit === true) {
          this.service.updateClient(this.idClient, client).then(rsp => {
            this.succes.emit(true);
            this.clearData();
          }, err => {
            this.errors = err;
            this.setErrors();
            this.errors = null;
            this.openDialog();
          });
        }

        if (this.addoredit === false) {
          this.service.addClient(client).pipe(first()).subscribe(rsp => {
            this.succes.emit(true);
            this.clearData();
          }, err => {
            this.errors = err;
            this.setErrors();
            this.errors = null;
            this.openDialog();
          });
        }
      }
    });
  }

  private clearData() {
    this.clientName = null;
    this.telephoneNumber = null;
    this.email = null;
    this.nameError = null;
    this.telephoneNumberError = null;
    this.emailError = null;
    this.errors = null;
  }

  private setErrors() {
    this.telephoneNumberError = this.errors.find(x => x.propertyName === 'TelephoneNumber')
      ?
      this.errors.find(x => x.propertyName === 'TelephoneNumber').errorMessage : undefined;
    this.emailError = this.errors.find(x => x.propertyName === 'Email')
      ? this.errors.find(x => x.propertyName === 'Email').errorMessage : undefined;
    this.nameError = this.errors.find(x => x.propertyName === 'ClientName')
      ? this.errors.find(x => x.propertyName === 'ClientName').errorMessage : undefined;
  }

}



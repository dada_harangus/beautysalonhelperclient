import {Component, OnInit} from '@angular/core';
import {ClientService} from '../../../Services/client.service';
import {ActivatedRoute} from '@angular/router';
import {Appointment} from '../../../Models/Appointment';
import {ClientAddModel} from '../../../Models/Dtos/ClientAddModel';
import {Location} from '@angular/common';
import {MonthReport} from '../../../Models/MonthReport';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-client-detail',
  templateUrl: './client-detail.component.html',
  styleUrls: ['./client-detail.component.css']
})
export class ClientDetailComponent implements OnInit {
  edit = 'Edit';
  client: ClientAddModel;
  history: Appointment [];
  future: Appointment[];
  monthReports: MonthReport[];


  constructor(private service: ClientService,
              private route: ActivatedRoute,
              private location: Location,
              private deleteAlert: MatSnackBar
  ) {
  }

  async ngOnInit() {

    await this.getClient();
    this.getMonthReports();
    this.getAppointments();
    this.getFutureAppointment();
  }


  async getClient(): Promise<void> {
    const id = +this.route.snapshot.paramMap.get('id');
    console.log(id);
    this.client = await this.service.getClient(id);
  }

  deleteClient(id: number, confirmed: boolean) {
    if (confirmed) {
      this.service.deleteClient(id).then(rsp => {
        if (rsp === false) {
          this.deleteAlert.open('Can`t delete client because it has appointments . Delete appointment first');
        }
        this.location.back();
      }, err => {
        console.log(err);
      });
    }
  }


  private getAppointments() {
    this.service.getAppointments(this.client.id).then(rsp =>
      this.history = rsp
    );
  }

  private getFutureAppointment() {
    this.service.getFutureAppointments(this.client.id).then(rsp =>
      this.future = rsp);
  }

  private getMonthReports() {
    this.service.getMonthlyReports(this.client.id).then(rsp =>
      this.monthReports = rsp);
  }

  refresh() {
    this.getClient();
  }
}

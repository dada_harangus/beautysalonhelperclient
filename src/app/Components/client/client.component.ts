import {Component, OnInit} from '@angular/core';
import {ClientService} from '../../Services/client.service';
import {Client} from '../../Models/Client';
import {Router} from '@angular/router';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {
  clients: Client[];
  add = 'Add';
  options: string[];


  constructor(private  service: ClientService, private router: Router) {
  }

  ngOnInit() {
    this.getClients();
  }

  getClients() {
    this.service.getClientList().then(rsp => {
      this.clients = rsp;

    }, err => {
      console.log('error', err);
    });
  }

  refresh() {
    this.getClients();
  }

  search(searchTerm: string) {
    this.service.search(searchTerm).then(rsp =>
      this.options = rsp.map(x => x.clientName));
  }

  goToClient(value: string) {
    this.service.search(value).then(rsp => {

      const idClient = rsp.find(x => x.clientName === value) ? rsp.find(x => x.clientName === value).id : undefined;

      this.router.navigate(['/detail/', idClient]);
    });
  }
}

import {Component, Renderer2, ViewChild} from '@angular/core';
import {MatCalendar, MatDialog, MatSnackBar} from '@angular/material';
import {Moment} from 'moment';
import {Employee} from '../../Models/Employee';
import {ClientAddModel} from '../../Models/Dtos/ClientAddModel';
import {Service} from '../../Models/Service';
import {DialogAppoinmentInputComponent} from './dialogAppoiment/dialogAppoinmentInput';
import * as moment from 'moment';
import {AppointmentService} from '../../Services/appointment.service';
import {AppointmentAddModel} from '../../Models/Dtos/AppointmentAddModel';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'appointment-component',
  templateUrl: './appointment.component.html',
  styleUrls: ['./appointment.component.css']
})

export class AppointmentComponent {
  // @ts-ignore
  @ViewChild('calendar') calendar: MatCalendar<Moment>;
  selectedDate: string;
  employee: Employee;
  client: ClientAddModel;
  service: Service[];
  startTime: any;
  endTime: any;
  name = JSON.parse(localStorage.getItem('currentUser')).username;
  employeeError: any;
  clientError: any;
  serviceError: any;
  startTimeError: any;
  endTimeError: any;
  errors: any;

  constructor(private renderer: Renderer2, public dialog: MatDialog,
              private  serviceAppointment: AppointmentService,
              private deleteAlert: MatSnackBar) {
  }


  onDateChanged(date) {
    this.selectedDate = moment(date).format('DD/MM/YYYY');
    console.log(this.selectedDate);
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogAppoinmentInputComponent, {
        width: '1200px',
        data: {
          employee: this.employee, client: this.client, service: this.service, startTime: this.startTime,
          endTime: this.endTime, selectedDate: this.selectedDate, name: this.name,
          clientError: this.clientError, employeeError: this.employeeError, serviceError: this.serviceError,
          startTimeError: this.startTimeError, endTimeError: this.endTimeError
        }
      })
    ;

    dialogRef.afterClosed().subscribe(async result => {
      if (result !== undefined) {
        this.client = result.client;
        console.log(this.client);
        this.employee = result.employee;
        this.service = result.service;
        this.endTime = result.endTime;
        this.startTime = result.startTime;
        const finalStartTime = this.selectedDate + ' ' + this.startTime;
        const finalEndTtime = this.selectedDate + ' ' + this.endTime;
        const appointement = {
          client: this.client,
          employee: this.employee,
          services: this.service,
          startTime: finalStartTime,
          endTime: finalEndTtime
        } as AppointmentAddModel;
        await this.serviceAppointment.addAppointment(appointement).then(rsp => {
          if (rsp === false) {
            this.deleteAlert.open('Can`t add appointment because the employee or is busy.');
          }

          this.clearData();
        }, err => {
          this.errors = err;
          console.log(this.errors);
          this.setErrors();
          this.openDialog();
        });
      }
      this.clearData();
    });
  }

  private clearData() {
    this.employeeError = null;
    this.clientError = null;
    this.serviceError = null;
    this.startTimeError = null;
    this.endTimeError = null;
    this.employee = null;
    this.client = null;
    this.service = null;
    this.startTime = null;
    this.endTime = null;

  }

  private setErrors() {
    this.employeeError = this.errors.find(x => x.propertyName === 'Employee')
      ?
      this.errors.find(x => x.propertyName === 'Employee').errorMessage : undefined;

    this.clientError = this.errors.find(x => x.propertyName === 'Client')
      ?
      this.errors.find(x => x.propertyName === 'Client').errorMessage : undefined;
    this.serviceError = this.errors.find(x => x.propertyName === 'Services')
      ?
      this.errors.find(x => x.propertyName === 'Services').errorMessage : undefined;
    this.startTimeError = this.errors.find(x => x.propertyName === 'StartTime')
      ?
      this.errors.find(x => x.propertyName === 'StartTime').errorMessage : undefined;
    this.endTimeError = this.errors.find(x => x.propertyName === 'EndTime')
      ?
      this.errors.find(x => x.propertyName === 'EndTime').errorMessage : undefined;
  }

}


import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ClientAddModel} from '../../../Models/Dtos/ClientAddModel';
import {Employee} from '../../../Models/Employee';
import {Service} from '../../../Models/Service';
import {ClientService} from '../../../Services/client.service';
import {EmployeeService} from '../../../Services/employee.service';
import {ServiceService} from '../../../Services/service.service';
import {AppointmentService} from '../../../Services/appointment.service';
import {Appointment} from '../../../Models/Appointment';

@Component({
  selector: 'app-edit-appointment',
  templateUrl: './edit-appointment.component.html',
  styleUrls: ['./edit-appointment.component.css']
})
export class EditAppointmentComponent implements OnInit {
  @Input() appear: boolean;
  @Output() sendAppointment = new EventEmitter<Appointment>();

  clients: ClientAddModel[];
  employees: Employee[];
  services: Service[];
  selectedServices: Service[];
  selectedEmployee: Employee;
  selectedClient: ClientAddModel;
  @Input() serviceError: string;
  @Input() clientError: string;
  @Input() employeeError: string;

  constructor(private clientService: ClientService,
              private employeeService: EmployeeService,
              private service: ServiceService,
              private appointementService: AppointmentService) {
  }

  ngOnInit() {
    this.getClients();
    this.getEmployees();

  }

  getEmployees() {
    this.employeeService.getEmployeeList().then(rsp => {
      this.employees = rsp;
    }, err => {
      console.log('error', err);
    });
  }

  getClients() {
    this.clientService.getClientList().then(rsp => {
      this.clients = rsp;

    }, err => {
      console.log('error', err);
    });
  }

  getServices(employee?: number) {
    return this.service.getServicesByEmployee(employee).then(rsp =>
      this.services = rsp);
  }

  upsert(selectedClient: ClientAddModel,
         selectedEmployee: Employee,
         selectedServices: Service[],
  ) {
    const appoinment = {
      client: selectedClient,
      employee: selectedEmployee,
      services: selectedServices,
      startTime: ' ',
      endTime: ''
    } as Appointment;
    this.appear = false;
    this.sendAppointment.emit(appoinment);

  }
}

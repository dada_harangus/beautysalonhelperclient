import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Service} from '../../../Models/Service';
import {ClientAddModel} from '../../../Models/Dtos/ClientAddModel';
import {Employee} from '../../../Models/Employee';
import {ClientService} from '../../../Services/client.service';
import {EmployeeService} from '../../../Services/employee.service';
import {ServiceService} from '../../../Services/service.service';
import {Appointment} from '../../../Models/Appointment';
import {AppointmentService} from '../../../Services/appointment.service';

export interface ModalData {
  employee: Employee;
  client: ClientAddModel;
  service: Service[];
  startTime: any;
  endTime: any;
  selectedDate: string;
  name: string;
  employeeError: any;
  clientError: any;
  serviceError: any;
  startTimeError: any;
  endTimeError: any;


}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'dialog-appointment-input',
  templateUrl: 'dialogAppoinmentInput.html',
  styleUrls: ['.alert{\n' +
  '  color: red;\n' +
  '\n' +
  '}']
})
// tslint:disable-next-line:component-class-suffix
export class DialogAppoinmentInputComponent implements OnInit {
  timeTabel: string[] = [
    '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00'

  ];
  clients: ClientAddModel[];
  employees: Employee[];
  services: Service[];
  appointments: Appointment[];
  selectedAppointment: Appointment;
  appear: boolean;
  employeeError: any;
  clientError: any;
  serviceError: any;
  startTimeError: any;
  endTimeError: any;
  errors: any[];

  ngOnInit(): void {
    this.getClients();
    this.getEmployees();
    this.getAppointementsByDay(this.data.selectedDate);

  }

  constructor(
    public dialogRef: MatDialogRef<DialogAppoinmentInputComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ModalData,
    private clientService: ClientService,
    private employeeService: EmployeeService,
    private service: ServiceService,
    private appointementService: AppointmentService) {
  }

  getEmployees() {
    this.employeeService.getEmployeeList().then(rsp => {
      this.employees = rsp;
    }, err => {
      console.log('error', err);
    });
  }

  getClients() {
    this.clientService.getClientList().then(rsp => {
      this.clients = rsp;

    }, err => {
      console.log('error', err);
    });
  }

  getServices(employee?: number) {
    return this.service.getServicesByEmployee(employee).then(rsp =>
      this.services = rsp);
  }


  onNoClick(): void {
    this.dialogRef.close();
  }


  private getAppointementsByDay(date: any) {
    this.appointementService.getAppointmentsByDate(date).then(rsp =>
      this.appointments = rsp
    );
  }

  selectAppointment(appointment: Appointment) {
    this.selectedAppointment = appointment;
    this.appear = true;
    console.log(this.selectedAppointment);
  }

  upsertAppointment(appointmentOld: Appointment, $event: Appointment) {
    $event.startTime = appointmentOld.startTime;
    $event.endTime = appointmentOld.endTime;
    console.log(appointmentOld.id);
    this.appointementService.upsertAppointment(appointmentOld.id, $event).then(rsp => {
        this.getAppointementsByDay(this.data.selectedDate);
        this.clearData();
      }, err => {
        this.errors = err;
        this.setErrors();
        console.log(this.errors);

      }
    );
  }

  deleteAppointment(id: number, confirmed: boolean) {
    if (confirmed) {
      this.appointementService.deleteAppointment(id).then(rsp =>
        this.getAppointementsByDay(this.data.selectedDate));
    }
  }

  private clearData() {
    this.employeeError = null;
    this.clientError = null;
    this.serviceError = null;
    this.startTimeError = null;
    this.endTimeError = null;

  }

  private setErrors() {
    this.employeeError = this.errors.find(x => x.propertyName === 'Employee')
      ?
      this.errors.find(x => x.propertyName === 'Employee').errorMessage : undefined;

    this.clientError = this.errors.find(x => x.propertyName === 'Client')
      ?
      this.errors.find(x => x.propertyName === 'Client').errorMessage : undefined;
    this.serviceError = this.errors.find(x => x.propertyName === 'Services')
      ?
      this.errors.find(x => x.propertyName === 'Services').errorMessage : undefined;
    this.startTimeError = this.errors.find(x => x.propertyName === 'startTime')
      ?
      this.errors.find(x => x.propertyName === 'startTime').errorMessage : undefined;
    this.endTimeError = this.errors.find(x => x.propertyName === 'endTime')
      ?
      this.errors.find(x => x.propertyName === 'endTime').errorMessage : undefined;
  }
}

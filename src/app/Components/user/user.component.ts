import {Component, OnInit} from '@angular/core';
import {UserService} from '../../Services/userService';
import {User} from '../../Models/User';

export interface DialogData {

  name: string;
  error: any;
  role: string;
}

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  users: User[];
  name = JSON.parse(localStorage.getItem('currentUser')).username;
  error: any;
  role: string;
  roleCurrentUser = JSON.parse(localStorage.getItem('currentUser')).role;
  userSelected: User;

  constructor(public dialog: MatDialog,
              private service: UserService,
              private deleteAlert: MatSnackBar,
              private location: Location) {
  }

  ngOnInit() {
    this.getUsers();
  }

  openDialog(): void {

    const dialogRef = this.dialog.open(DialogChangeRoleComponent, {
      width: '250px',
      data: {
        name: this.name,
        error: this.error,
        role: this.role,
      }
    });

    dialogRef.afterClosed().subscribe(result => {

      this.error = result.error;
      this.role = result.role;
      console.log(this.role);
      if (result !== undefined) {
        this.service.changeRole(this.userSelected.id, this.role).then(rsp => {
            this.getUsers();
          },
          err => {
            this.error = err;
          });
      }
    });
  }

  getUsers() {
    this.service.getAll().then(rsp => {
      this.users = rsp;

    }, err => {
      console.log('error', err);
    });
  }

  selectUser(user: User) {
    this.userSelected = user;
    console.log(this.userSelected);
  }

  deleteUser(id: number, confirmed: boolean) {
    if (confirmed) {
      this.service.deleteClient(id).then(rsp => {
        if (rsp === false) {
          this.deleteAlert.open('Can`t delete client because it has appointments . Delete appointment first');
        }
        this.location.back();
      }, err => {
        console.log(err);
      });
    }

  }
}

import {Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {Location} from '@angular/common';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'dialog-changeRole-input',
  templateUrl: 'dialogChangeRole.html',
  styleUrls: ['user.component.css']
})
// tslint:disable-next-line:component-class-suffix
export class DialogChangeRoleComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogChangeRoleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../Services/AuthenticationService';
import {RegisterPostModel} from '../../Models/registerPostModel';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  hide = true;
  firstName: string;
  lastName: string;
  username: string;
  password: string;

  constructor(private authenticationService: AuthenticationService, private router: Router) {
  }

  ngOnInit() {
  }

  onSubmit() {
    const register = {
      firstName: this.firstName,
      lastName: this.lastName,
      username: this.username,
      password: this.password
    } as RegisterPostModel;

    this.authenticationService.register(register).then(rsp =>
      this.authenticationService.login(register.username, register.password).subscribe(rsp =>
        this.router.navigate(['/client'])
      )
    )
    ;

  }
}

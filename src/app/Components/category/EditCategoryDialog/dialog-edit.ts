import {Component, EventEmitter, Input, Output,} from '@angular/core';
import {MatDialog} from '@angular/material';
import {DialogEditInputComponent} from './dialog-Edit-input';
import {Category} from '../../../Models/Category';
import {CategoryService} from '../../../Services/category.service';

export interface DialogData {
  categoryName: string;
  name: string;
  categoryNameError: any;

}

/**
 * @title Dialog Overview
 */
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'dialog-edit',
  templateUrl: 'dialog-Edit.html',
  styleUrls: ['dialog-Edit.css'],
})
// tslint:disable-next-line:component-class-suffix
export class DialogEditComponent {

  categoryName: string;
  name = JSON.parse(localStorage.getItem('currentUser')).username;
  categoryNameError: any;
  errors: any[];
  @Input() categoryId: number;
  @Output() succes = new EventEmitter<boolean>();
  @Input() addOrEdit: boolean;

  constructor(private dialog: MatDialog, private service: CategoryService) {

  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogEditInputComponent, {
      width: '250px',
      data: {
        name: this.name,
        categoryName: this.categoryName,
        categoryNameError: this.categoryNameError
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.categoryName = result.categoryName;
      if (result !== undefined) {
        if (this.addOrEdit === false) {
          const categoryToSave = {id: 0, name: this.categoryName} as Category;
          this.service.addCategory(categoryToSave).then(rsp => {
              this.succes.emit(true);
              this.clearData();
            },
            err => {
              this.errors = err;
              this.setErros();
              this.errors = null;
              this.openDialog();
            });
        }

        if (this.addOrEdit === true) {
          const categoryToEdit = {id: this.categoryId, name: this.categoryName} as Category;
          this.service.updateCategory(this.categoryId, categoryToEdit).then(rsp => {
              this.succes.emit(true);
              this.clearData();
            },
            err => {
              this.errors = err;
              this.setErros();
              this.errors = null;
              this.openDialog();
            });
        }
      }
    });
  }

  clearData() {
    this.errors = null;
    this.categoryName = null;
    this.categoryNameError = null;
  }

  setErros() {
    this.categoryNameError = this.errors.find(x => x.propertyName === 'name')
      ?
      this.errors.find(x => x.propertyName === 'name').errorMessage : undefined;
  }
}

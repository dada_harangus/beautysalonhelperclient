import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DialogData} from './dialog-edit';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'dialog-Edit-input',
  templateUrl: 'dialog-Edit-input.html',
  styleUrls: ['dialog-Edit.css']
})
// tslint:disable-next-line:component-class-suffix
export class DialogEditInputComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogEditInputComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

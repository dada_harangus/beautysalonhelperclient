import {Component, OnInit} from '@angular/core';
import {CategoryService} from '../../Services/category.service';
import {Service} from '../../Models/Service';
import {CategoryWithServices} from '../../Models/CategoryWithServices';
import {ServiceService} from '../../Services/service.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  categories: CategoryWithServices[];
  services: Service[];


  constructor(private service: CategoryService,
              private serviceService: ServiceService,
              private deleteAlert: MatSnackBar) {
  }

  ngOnInit() {
    this.getCategories();
  }


  getCategories() {
    this.service.getCategoryList().then(rsp => {
      this.categories = rsp.map(value => new CategoryWithServices(value.id, value.name, []));
    }, err => {
      console.log('error', err);
    });
  }

  getServices(id: number) {
    this.service.getServices(id).then(rsp => {
      this.categories.find(value => value.id === id).services = rsp;

    });
  }


  deleteCategory(id: number, confirmed: boolean) {
    if (confirmed) {
      this.service.deleteCategory(id).then(rsp => {

        if (rsp === false) {
          this.deleteAlert.open('Can`t delete category because it has employees or services.');
        }

        this.getCategories();
      });

    }
  }


  deleteService(id: number, categoryId: number, confirmed: boolean) {
    if (confirmed) {
      this.serviceService.deleteService(id).then(rsp => {
        if (rsp === false) {
          this.deleteAlert.open('Can`t delete service because it has appointments . Delete appointment first');
        }
        this.getServices(categoryId);
      });
    }
  }


  refresh() {
    this.getCategories();
  }

  refreshService(id: number) {
    this.getServices(id);
  }
}

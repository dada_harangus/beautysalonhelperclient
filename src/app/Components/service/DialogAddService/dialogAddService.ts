import {Component, EventEmitter, Input, Output} from '@angular/core';
import {MatDialog} from '@angular/material';
import {DialogAddServiceInputComponent} from './dialogAddServiceInput';
import {ServiceService} from '../../../Services/service.service';

export interface DialogData {
  serviceName: string;
  servicePrice: number;
  serviceDuration: number;
  serviceNameError: any;
  servicePriceError: any;
  serviceDurationError: any;
  name: string;
}

/**
 * @title Dialog Overview
 */
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'dialog-addService',
  templateUrl: 'dialogAddService.html',
  styleUrls: ['dialogAddService.css'],
})
// tslint:disable-next-line:component-class-suffix
export class DialogAddServiceComponent {

  serviceName: string;
  servicePrice: number;
  serviceDuration: number;
  serviceNameError: any;
  servicePriceError: any;
  serviceDurationError: any;
  name = JSON.parse(localStorage.getItem('currentUser')).username;
  errors: any[];
  @Input() categoryId: number;
  @Input() serviceId: number;
  @Input() addOrEdit: boolean;
  @Output() succes = new EventEmitter<boolean>();

  constructor(public dialog: MatDialog, public service: ServiceService) {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogAddServiceInputComponent, {
      width: '250px',
      data: {
        name: this.name,
        serviceName: this.serviceName,
        servicePrice: this.servicePrice,
        serviceDuration: this.serviceDuration,
        serviceNameError: this.serviceNameError,
        servicePriceError: this.servicePriceError,
        serviceDurationError: this.serviceDurationError
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.serviceName = result.serviceName;
        this.servicePrice = result.servicePrice;
        this.serviceDuration = result.serviceDuration;
        const service = {name: this.serviceName, price: this.servicePrice, duration: this.serviceDuration, categoryId: this.categoryId};

        if (this.addOrEdit === true) {
          this.service.updateService(this.serviceId, service).then(rsp => {
            this.succes.emit(true);
            this.clearData();

          }, err => {
            this.errors = err;
            this.setErrors();
            this.openDialog();
          });
        }

        if (this.addOrEdit === false) {
          this.service.addService(service).then(rsp => {
            this.succes.emit(true);
            this.clearData();

          }, err => {
            this.errors = err;
            this.setErrors();
            this.openDialog();
          });
        }
      }
    });
  }

  clearData() {
    this.errors = null;
    this.serviceName = null;
    this.servicePrice = null;
    this.serviceDuration = null;
    this.serviceNameError = null;
    this.servicePriceError = null;
    this.serviceDurationError = null;
  }

  setErrors() {
    this.serviceNameError = this.errors.find(x => x.propertyName === 'Name')
      ?
      this.errors.find(x => x.propertyName === 'Name').errorMessage : undefined;
    this.servicePriceError = this.errors.find(x => x.propertyName === 'Price')
      ? this.errors.find(x => x.propertyName === 'Price').errorMessage : undefined;
    this.serviceDurationError = this.errors.find(x => x.propertyName === 'Duration')
      ? this.errors.find(x => x.propertyName === 'Duration').errorMessage : undefined;
    this.errors = null;
  }

}




import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DialogData} from './dialogAddService';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'dialogAddServiceInput',
  templateUrl: 'dialogAddServiceInput.html',
  styleUrls: ['dialogAddService.css'],
})
// tslint:disable-next-line:component-class-suffix
export class DialogAddServiceInputComponent {



  constructor(
    public dialogRef: MatDialogRef<DialogAddServiceInputComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }


  onNoClick(): void {
    this.dialogRef.close();
  }

}

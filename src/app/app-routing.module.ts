import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AppointmentComponent} from './Components/appointment/appointment.component';
import {EmployeeComponent} from './Components/employee/employee.component';
import {ClientComponent} from './Components/client/client.component';
import {CategoryComponent} from './Components/category/category.component';
import {ServiceComponent} from './Components/service/service.component';
import {ClientDetailComponent} from './Components/client/client-detail/client-detail.component';
import {EmployeeDetailComponent} from './Components/employee/employee-detail/employee-detail.component';
import {EditAppointmentComponent} from './Components/appointment/edit-appointment/edit-appointment.component';
import {LoginComponent} from './Components/login/login.component';
import {HomeComponent} from './Components/home/home.component';
import {AuthGuard} from './AuthGuard/AuthGuard';
import {RegisterComponent} from './Components/register/register.component';
import {UserComponent} from './Components/user/user.component';

const routes: Routes = [

  {path: '', component: HomeComponent, canActivate: [AuthGuard]},
  {path: 'appointment', component: AppointmentComponent, canActivate: [AuthGuard]},
  {path: 'employee', component: EmployeeComponent, canActivate: [AuthGuard]},
  {path: 'client', component: ClientComponent, canActivate: [AuthGuard]},
  {path: 'category', component: CategoryComponent, canActivate: [AuthGuard]},
  {path: 'service', component: ServiceComponent, canActivate: [AuthGuard]},
  {path: 'user', component: UserComponent, canActivate: [AuthGuard]},
  {path: 'register', component: RegisterComponent},
  {path: 'editAppointment', component: EditAppointmentComponent, canActivate: [AuthGuard]},
  {path: 'detail/:id', component: ClientDetailComponent, canActivate: [AuthGuard]},
  {path: 'detailEmployee/:id', component: EmployeeDetailComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent, },
  {path: '**', component: HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

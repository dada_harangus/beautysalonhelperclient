import {Component} from '@angular/core';
import {AuthenticationService} from './Services/AuthenticationService';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Beauty Salon Helper';

  constructor(private service: AuthenticationService) {
  }

  logout() {
    this.service.logout();
  }
}
